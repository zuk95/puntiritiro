package puntiRitiro;

import pacchi.Pacco;
import puntiRitiro.AbstractPuntoRitiro;

import java.util.ArrayList;

public class GestorePuntiRitiro {
    private ArrayList<AbstractPuntoRitiro> lista;
    private final int MAX_GIORNI = 3;

    public GestorePuntiRitiro() {
        this.lista=new ArrayList<>();
    }

    public boolean addPunto(AbstractPuntoRitiro a){
        return lista.add(a);
    }

    public ArrayList<AbstractPuntoRitiro> puntiDisponibili(Pacco p){
        ArrayList<AbstractPuntoRitiro> disponibili = new ArrayList<>();
        for (AbstractPuntoRitiro a:lista
             ) {
            if(a.available(p)){
                disponibili.add(a);
            }
        }
        System.out.println(disponibili);
        return disponibili;
    }

    public boolean addPacco(Pacco p,AbstractPuntoRitiro a){
        if(puntiDisponibili(p).contains(a)){
            return a.addPacco(p);
        }
        return false;
    }

    public void aumentoGiorni(int giorni){
        for (AbstractPuntoRitiro a:lista
             ) {
            a.aumentoGiorni(giorni);
        }
    }

    public void delPacchiOverDay(){
        for (AbstractPuntoRitiro a:lista
             ) {
            a.eliminaPaccoOverDay();
        }
    }


    @Override
    public String toString() {
        return "puntiRitiro.GestorePuntiRitiro{" +
                "lista=" + lista +
                '}';
    }
}
