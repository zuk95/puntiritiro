package puntiRitiro;

import deposito.DepositoLibero;
import pacchi.Pacco;

public class Hub extends AbstractPuntoRitiro {

    public Hub() {
        this.deposito = new DepositoLibero();
    }

    @Override
    public boolean addPacco(Pacco p) {
        if(deposito.addPacco(p)){
            p.setCodice(creaCodice());
            return true;
        }
        return false;
    }

    @Override
    public boolean rmvPacco(String cod) {
        return deposito.rmvPacco(cod);
    }

    @Override
    public String creaCodice() {
        return "HUB"+deposito.getOccupazione()+"Pacco";
    }


    @Override
    public String toString() {
        return "Hub{" +
                "deposito=" + deposito +
                '}';
    }
}
