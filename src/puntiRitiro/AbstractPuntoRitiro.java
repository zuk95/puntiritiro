package puntiRitiro;

import deposito.AbstractDeposito;
import pacchi.Pacco;

public abstract class AbstractPuntoRitiro {
    protected AbstractDeposito deposito;
    private boolean disponibilita;

    public AbstractPuntoRitiro() {
    }

    public abstract boolean addPacco(Pacco p);
    public abstract boolean rmvPacco(String cod);
    public abstract String creaCodice();


    public void aumentoGiorni(int giorni){
        deposito.aumentoGiorni(giorni);
    }

    public void eliminaPaccoOverDay(){
        deposito.eliminaPaccoOverDay();
    }

    public boolean available(Pacco p){
        return deposito.controlloDisponibilita(p);
    }

}
