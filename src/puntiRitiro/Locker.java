package puntiRitiro;

import deposito.DepositoBox;
import pacchi.Pacco;

public class Locker extends AbstractPuntoRitiro {
    public Locker() {
        this.deposito=new DepositoBox();
    }

    @Override
    public boolean addPacco(Pacco p) {
        if(deposito.addPacco(p)){
            p.setCodice(creaCodice());
            return true;
        }
        return false;
    }

    @Override
    public boolean rmvPacco(String cod) {
        return deposito.rmvPacco(cod);
    }

    @Override
    public String creaCodice() {
        return "LOCKER"+deposito.getOccupazione()+"Pack";
    }

    @Override
    public String toString() {
        return "Locker{" +
                "deposito=" + deposito +
                '}';
    }
}
