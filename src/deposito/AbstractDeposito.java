package deposito;

import pacchi.Pacco;

public abstract class AbstractDeposito {
    private boolean disponibilita;
    private int occupazione;

    public AbstractDeposito() {
        this.disponibilita=true;
        this.occupazione=0;
    }

    public int getOccupazione() {
        return occupazione;
    }

    protected void occPiu(){
        occupazione++;
    }
    protected void occMeno(){
        occupazione--;
    }

    public abstract boolean addPacco(Pacco p);
    public abstract boolean rmvPacco(String cod);
    public abstract boolean controlloDisponibilita(Pacco p);
    public abstract void aumentoGiorni(int giorni);
    public abstract void eliminaPaccoOverDay();


    @Override
    public String toString() {
        return "AbstractDeposito{" +
                "disponibilita=" + disponibilita +
                ", occupazione=" + occupazione +
                '}';
    }
}
