package deposito;

import pacchi.Pacco;

import java.util.ArrayList;

public class DepositoLibero extends AbstractDeposito {
    private ArrayList<Pacco> deposito;

    public DepositoLibero() {
        this.deposito=new ArrayList<>();
    }

    @Override
    public boolean addPacco(Pacco p) {
        occPiu();
        return deposito.add(p);

    }

    @Override
    public boolean rmvPacco(String cod) {
        for (Pacco p:deposito
             ) {
            if(p.getCodice()==cod){
                occMeno();
               return deposito.remove(p);
            }
        }
        return false;
    }

    @Override
    public boolean controlloDisponibilita(Pacco p) {
        return true;
    }

    @Override
    public void aumentoGiorni(int giorni) {
        for (Pacco p:deposito
             ) {
            p.settaGiorno(giorni);
        }
    }

    @Override
    public void eliminaPaccoOverDay() {
        for (Pacco p:deposito
             ) {
                if (p.getGiorno() > 3) {
                    deposito.remove(p);
                    occMeno();
                    if (deposito.isEmpty()) {
                        break;
                    }
                }
        }
    }

    @Override
    public String toString() {
        return "DepositoLibero{" +
                "deposito=" + deposito + super.toString()+
                '}';
    }
}
