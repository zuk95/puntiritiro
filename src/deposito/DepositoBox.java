package deposito;

import pacchi.Box;
import pacchi.Pacco;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class DepositoBox extends AbstractDeposito {
    private ArrayList<Box> deposito;
    private final int NUM_BOX=3;

    public DepositoBox() {
        this.deposito=new ArrayList<>();
        System.out.println("INSERIRE DIMENSIONE BOX");
        for(int i=0;i<NUM_BOX;i++){
            deposito.add(new Box(new Scanner(System.in).nextInt()));
        }
        Collections.sort(deposito);

    }

    public boolean controlloDisponibilita(Pacco p){
        if(getOccupazione()< NUM_BOX && controlloDispDim(p)){
            return true;
        }else{
            return false;
        }
    }

    public boolean controlloDispDim(Pacco p){
        for (Box b:deposito
             ) {
            if(b.getDimensione()>p.getDimensione()&&b.getPacco()== null){
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean addPacco(Pacco p) {
        if(controlloDisponibilita(p)){
            for (Box b:deposito
                 ) {
                    if(b.addPacco(p)){
                        occPiu();
                        return true;
                    }
            }
            return false;
        }
        return false;
    }

    @Override
    public void aumentoGiorni(int giorni) {
        for (Box b: deposito
             ) {
            if(b.getPacco()!=null){
                b.getPacco().settaGiorno(giorni);
            }
        }
    }

    @Override
    public void eliminaPaccoOverDay() {
        for (Box b:deposito
             ) {
            if(b.getPacco()!=null) {
                if (b.getPacco().getGiorno() > 3) {
                    b.setPacco(null);
                    occMeno();
                }
            }
        }
    }

    @Override
    public boolean rmvPacco(String cod) {
        for (Box b:deposito
             ) {
            if(b.getPacco()!=null && b.getPacco().getCodice()==cod){
                occMeno();
                return b.rmvPacco(cod);
            }
        }
        return false;
    }


    @Override
    public String toString() {
        return "DepositoBox{" +
                "deposito=" + deposito +
                '}';
    }
}
