package pacchi;

public class Pacco {
    private int dimensione;
    private String codice;
    private int giorno;

    public Pacco(int dimensione) {
        this.dimensione = dimensione;
        this.giorno=0;
    }

    public int getGiorno() {
        return giorno;
    }

    public void setDimensione(int dimensione) {
        this.dimensione = dimensione;
    }

    public void settaGiorno(int giorni){
        this.giorno=giorni;
    }

    public int getDimensione() {
        return dimensione;
    }

    public void setCodice(String codice) {
        this.codice = codice;
    }

    public String getCodice() {
        return codice;
    }

    @Override
    public String toString() {
        return "Pacco{" +
                "dimensione=" + dimensione +
                ", codice='" + codice + '\'' +
                ", giorno=" + giorno +
                '}';
    }
}
