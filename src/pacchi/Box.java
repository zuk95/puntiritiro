package pacchi;

public class Box implements Comparable<Box>{
    private int dimensione;
    private Pacco pacco;

    public Box(int dimensione) {
        this.dimensione = dimensione;
        this.pacco=null;
    }

    public void setDimensione(int dimensione) {
        this.dimensione = dimensione;
    }

    public boolean addPacco(Pacco p){
        if(p.getDimensione()<=dimensione && pacco==null){
            pacco=p;
            return true;
        }else{
            return false;
        }
    }

    public void setPacco(Pacco pacco) {
        this.pacco = pacco;
    }

    public void delPaccoOverDay(){
        if(pacco.getGiorno()>3){
            pacco=null;
        }
    }

    public void settaGiorno(int giorni){
        if(pacco!=null){
            pacco.settaGiorno(giorni);
        }
    }

    public int getDimensione() {
        return dimensione;
    }

    public boolean rmvPacco(String cod){
        if(cod == pacco.getCodice()){
            pacco = null;
            return true;
        }else{
            return false;
        }
    }

    @Override
    public int compareTo(Box o) {
        int diff = this.dimensione - o.getDimensione();
        return diff;
    }

    public Pacco getPacco() {
        return pacco;
    }

    @Override
    public String toString() {
        return "Box{" +
                "dimensione=" + dimensione +
                ", pacco=" + pacco +
                '}';
    }
}
