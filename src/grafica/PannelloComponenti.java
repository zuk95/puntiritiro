package grafica;

import pacchi.Pacco;
import puntiRitiro.GestorePuntiRitiro;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class PannelloComponenti extends JPanel {
    private DimPacco dimPacco;
    private ButtonAvvia buttonAvvia;
    private Schermo schermo;
    private Pacco pacco;
    private GestorePuntiRitiro gestore;

    public PannelloComponenti() {
        BorderLayout layout = new BorderLayout();
        setLayout(layout);
        this.dimPacco = new DimPacco();
        this.buttonAvvia = new ButtonAvvia();
        this.schermo = new Schermo();
        buttonAvvia.addActionListener(al);
        add(dimPacco,BorderLayout.NORTH);
        add(buttonAvvia,BorderLayout.SOUTH);
        add(schermo,BorderLayout.CENTER);

    }

    ActionListener al = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            String valore = dimPacco.getText();
            int dim = Integer.parseInt(valore);
            dimPacco.setDim(dim);
            pacco = new Pacco(dim);
            schermo.setText(gestore.puntiDisponibili(pacco).toString());
        }
    };

    public void setGestore(GestorePuntiRitiro gestore) {
        this.gestore = gestore;
    }
}
