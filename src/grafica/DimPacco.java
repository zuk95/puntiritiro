package grafica;

import javax.swing.*;

public class DimPacco extends JTextField {
    private int dim;

    public DimPacco() {
        setSize(50,30);
    }

    public int getDim() {
        return dim;
    }

    public void setDim(int dim) {
        this.dim = dim;
    }
}
