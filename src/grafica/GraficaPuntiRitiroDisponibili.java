package grafica;

import puntiRitiro.GestorePuntiRitiro;

import javax.swing.*;
import java.awt.*;

public class GraficaPuntiRitiroDisponibili extends JFrame {
    public GraficaPuntiRitiroDisponibili(GestorePuntiRitiro gestore){
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);
        setSize(500,500);
        PannelloComponenti pannello = new PannelloComponenti();
        pannello.setGestore(gestore);
        setContentPane(pannello);
    }
}
