import grafica.GraficaPuntiRitiroDisponibili;
import pacchi.Pacco;
import puntiRitiro.AbstractPuntoRitiro;
import puntiRitiro.GestorePuntiRitiro;
import puntiRitiro.Hub;
import puntiRitiro.Locker;

public class Tester {
    public static void main(String[] args) {
        AbstractPuntoRitiro locker = new Locker();//10,50,100
        AbstractPuntoRitiro hub = new Hub();
        GestorePuntiRitiro gestore = new GestorePuntiRitiro();
        gestore.addPunto(locker);
        gestore.addPunto(hub);
        gestore.addPacco(new Pacco(30),locker);
        gestore.addPacco(new Pacco(30),hub);

        gestore.aumentoGiorni(5);

        System.out.println(gestore);

        gestore.delPacchiOverDay();

        System.out.println(gestore);

        gestore.addPacco(new Pacco(5),locker);
        gestore.delPacchiOverDay();

        System.out.println(gestore);

        gestore.addPacco(new Pacco(20),locker);
        gestore.addPacco(new Pacco(100),hub);

        gestore.aumentoGiorni(10);

        System.out.println(gestore);
        gestore.delPacchiOverDay();
        System.out.println(gestore);

        gestore.addPacco(new Pacco(20),locker);
        gestore.addPacco(new Pacco(30),locker);
        System.out.println(gestore);

        gestore.puntiDisponibili(new Pacco(80));

        GraficaPuntiRitiroDisponibili gui = new GraficaPuntiRitiroDisponibili(gestore);
        gui.setVisible(true);


    }
}
